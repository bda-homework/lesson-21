import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Character character = new Character();
        Scanner scanner = new Scanner(System.in);

        Thread decreaseHealthThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (character.getHealth() >= 1.0) {
                    character.decreaseHealth();
                    if (character.getHealth() < 60.0) {
                        System.out.println("Character is hungry");
                    }
                    if (character.getHealth() < 30.0) {
                        System.out.println("Critical condition of the character");
                    }
                    if (character.getHealth() < 1.0) {
                        System.out.println("Character is dead");
                        System.exit(0);
                    }
                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        decreaseHealthThread.start();

        while (true) {
            showMenu();
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    character.eatMeal();
                    break;
                case 2:
                    character.drinkMilk();
                    break;
                case 3:
                    character.showEnergy();
                    break;
                default:
                    System.out.println("Wrong choice");
            }
        }
    }

    private static void showMenu() {
        System.out.println("Choose an action:");
        System.out.println("[1]. Eat meal");
        System.out.println("[2]. Drink milk");
        System.out.println("[3]. Show energy");
        System.out.print("Enter your choice: ");
    }

}