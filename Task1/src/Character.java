public class Character {
    private double energy = 100.0;

    public double getHealth() {
        return energy;
    }

    public void eatMeal() {
        energy *= 1.09;
    }

    public void drinkMilk() {
        energy *= 1.08;
    }

    public void showEnergy() {
        System.out.println("Energy: " + energy + "%");
    }

    public void decreaseHealth() {
        energy *= 0.91;
    }
}
